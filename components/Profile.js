import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Profile extends React.Component {
  render() {
    return (
      <View style={styles.container}>
<Text>Hi Profile</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button:{
    backgroundColor:'red',
    alignItems:'center',
    width:260,
  },
  buttonText:{
    color:'white',
    padding:20
  }
});
