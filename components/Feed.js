import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import {createStackNavigator} from 'react-navigation';

export default class Feed extends React.Component {
    
  render() {
    const username =  this.props.navigation.getParam('username','No Username')
    return (
      <View style={styles.container}>
      <Text>Hi Feed</Text>
      <Text>Username = {username}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button:{
    backgroundColor:'red',
    alignItems:'center',
    width:260,
  },
  buttonText:{
    color:'white',
    padding:20
  }
});
