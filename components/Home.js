import React from 'react';
import { StyleSheet, Text, View,Button } from 'react-native';

export default class Home extends React.Component {
  render() {
    return (
      <View style={styles.container}>
      <Text>Hi Home</Text>
      <Button title='Go to Profile' style={{marginBottom:'15px'}} onPress={()=> this.props.navigation.navigate('Profile')}/>
      
      <Button title='Go to Feed' onPress={()=> this.props.navigation.navigate('Feed',{username:'mark'})}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button:{
    backgroundColor:'red',
    alignItems:'center',
    width:260,
  },
  buttonText:{
    color:'white',
    padding:20
  }
});
